import { useState, useEffect } from "react";
import axios from "axios";
import {Form, FormControl, Button, Modal} from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.css"
import paginationFactory from "react-bootstrap-table2-paginator";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css"
import { Container,Row,Col,Card,ListGroup,ListGroupItem } from "react-bootstrap";


const MainTableFrame = () => {
    const [data, setData] = useState([]);
    const [query, setQuery] = useState("");
    const [modalInfo, setModalInfo] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        axios.get("https://employeesapitable.herokuapp.com/api")
            .then( response => {
                setData(response.data.employees);
            })
            .catch(err => console.log('fetch problem while fetching api'))
    }, [])

    const columns = [
        {dataField: 'id', text: 'ID'.substring(0, 4)},
        {dataField: 'firstName', text: 'Name', sort: true},
        {dataField: 'contactNo', text: 'Contact No', sort: true},
        {dataField: 'address', text: 'Address', sort: true},

    ]
    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 5,
        lastPageText: ">>",
        firstPageText: "<<",
        nextPageText: ">",
        prePageText: "<",
        showTotal: true,
        alwaysShowAllBtns: true,
        onPageChange: (page, sizePerPage) => {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        },
        onSizePerPageChange: (page, sizePerPage) => {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        }
    })

    const search = (rows) => {
        const columns = rows[0] && Object.keys(rows[0])      
        return rows.filter(row => columns.some(column => row[column].toString().toLowerCase().indexOf(query.toLocaleLowerCase()) > -1))
    }

    const rowEvents = {
        onClick: (e, row) => {
            setModalInfo(row);
            toggleTrueFalse();
        }
    }

    const toggleTrueFalse = () => {
        setShowModal(handleShow);
    }

    const ModalContent = (props) => {
        return (
            <Modal
              {...props}
              size="lg"
              aria-labelledby="contained-modal-title-vcenter"
              centered
            >
              <Modal.Header closeButton />
              <Modal.Body>
                <Container>
                    <Row>
                        <Col xs={6} md={4}>
                        <Card>
                            <Card.Img variant="top" src={`${modalInfo.avatar}/100px180?text=Image cap`} />
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>Job title: {modalInfo.jobTitle}</ListGroupItem>
                                <ListGroupItem>Age: {modalInfo.age}</ListGroupItem>
                                <ListGroupItem>Joined: {new Date(modalInfo.dateJoined).getMonth()}/{new Date(modalInfo.dateJoined).getDay()}/{new Date(modalInfo.dateJoined).getFullYear()}</ListGroupItem>
                            </ListGroup>
                        </Card>
                        </Col>
                        <Col xs={12} md={8}>
                            <h2>{modalInfo.firstName} {modalInfo.lastName}</h2>
                            <hr />
                            <p>{modalInfo.bio}</p>
                        </Col>
                    </Row>
                </Container>
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
              </Modal.Footer>
            </Modal>
          );
    }

    return (
        <>
            <Container className="mt-5">
                <Form className="d-flex">
                    <FormControl
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        value={query}
                        onChange={(e) => setQuery(e.target.value)}
                        />
                    <Button variant="outline-success">Search</Button>
                </Form>
                    <br />
                    <BootstrapTable 
                        rowEvents={rowEvents}
                        bootstrap4 
                        keyField="id"
                        columns={columns} 
                        data={search(data)} 
                        pagination={pagination} 
                        wrapperClasses="table-responsive"
                        />
                        {show ? <ModalContent show={show} onHide={handleClose} /> : null}
            </Container>
        </>
    )
}

export default MainTableFrame;