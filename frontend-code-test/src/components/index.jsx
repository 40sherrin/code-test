import Navbar from "./Navbar";
import MainFrame from "./MainTableFrame";

const App = () => {
    return (
        <>
            <Navbar />
            <MainFrame />
        </>
    )
}

export default App;