import { useState, useEffect } from "react";
import axios from "axios";
import {Navbar, Container, Nav} from "react-bootstrap";



const CustomFilteredDate = ({companyEst}) => {
    const day = new Date(companyEst).getDay();
    const month = new Date(companyEst).getMonth();
    return (
        <>
            <span>
                {month < 10 ? `0${month}` : month}/{day <= 0 && day < 10 ? `0${day + 1}` : day }/{new Date(companyEst).getFullYear()}
            </span>
        </>
    )
}

const NavbarSection = () => {
    const [companydetails, setCompanyDetails] = useState({});
    
    useEffect(() => {
        axios.get('https://employeesapitable.herokuapp.com/api').then(res => {
            setCompanyDetails(res.data.companyInfo)
        }).catch(err => console.error(err))
    },[])
    
    const {companyName, companyMotto, companyEst} = companydetails;

    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                <Navbar.Brand href="#home"><h1>{companyName}</h1> <br /> <p>{companyMotto}</p></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto">
                                    <Navbar.Text></Navbar.Text>
                            </Nav>
                            <Nav style={{
                                marginTop: "5.5rem",
                                color: '#fff'
                            }}>
                                    <Navbar.Text>Since ({<CustomFilteredDate companyEst={companyEst} />})</Navbar.Text>
                            </Nav>
                        </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
};

export default NavbarSection;