const express = require('express');
const app = express();
const cors = require('cors');
const PORT = process.env.PORT || 8080;
const fs = require('fs');
const path = require('path');
const pathToFile = path.resolve("./sample-data.json");

// const corsOptions = {
//     origin: 'http://localhost:3000',
//     optionSuccessStatus: 200
// }

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// }); 

app.use(cors())

const getApi = () => JSON.parse(fs.readFileSync(pathToFile));

app.get('/', (req, res) => {
    return res.send('check /api route for more details')
})

app.get('/api', (req, res) => {
    const resources = getApi();
    return res.send(resources)
})


app.listen(PORT, () => console.log(`check ====> http://localhost:${PORT}`))

